# Build and Push images to GitLab Registry & DockerHub using Kaniko & GitLab-CI

## What is Martor and what is the purpose of this project?

**Martor** basically is a Markdown Editor plugin for Django, supported for Bootstrap and Semantic-UI. To me it looked like a great open source project to use as the service my container provides rather than our regular hello-world container. Markor makes things slightly more interesting but the main purpose of this project is just to get my hands dirty with [Kaniko](https://github.com/GoogleContainerTools/kaniko) and Gitlab-CI.

For further details about Markor you should check out the [preview](https://python.web.id/try-martor-online/) and the [project on Github](https://github.com/agusmakmun/django-markdown-editor).

## Test Martor container locally using good old Docker Engine and Dockerfile

For now, the easiest way I know to make sure the Dockerfile works as intended is building and running the container locally. Checkout the project in any directory on your computer and navigate to the project root folder...

1. Build the image locally using Docker Engine:

   ```shell
   docker image build --tag martor-demo .
   ```

2. Run the *martor-demo* container:

   ```shell
   docker container run -it -d -p 8000:8000 --name martor-demo martor-demo
   ```

3. Check if the django application has started in the container (expect to see below output):

    ```shell
    $ docker logs -f martor-demo

    ...
    ...
    Django version 3.2.4, using settings 'martor_demo settings'
    Starting development server at http://0.0.0.0:8000/
    Quit the server with CONTROL-C.
    ```

4. Check if you reach use the app locally (<http://127.0.0.1:8000/simple-form>):

   ![markor-preview](docs/markor-preview.jpg?raw=true)

If you can reach out to the Markor application locally, the news are good! The image still works and we are ready to start automating the build and push stages.

## Build and Push the image using Kaniko & GitLab-CI

### What is Kaniko and why to use it?

**Kaniko** is a tool to build container images from a Dockerfile inside a container or Kubernetes cluster and to push the build to a registry. 

Kaniko doesn't require the Docker daemon and unlike the Docker-in-Docker build method it does not require [runtime privileges](https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities). (Check out the interesting [article](https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/) about the privileged mode).

## .gitlab-ci.yml Overview

Now, we are sure that the Dockerfile is working as intended and we can build and push our image to GitLab Registry and to Dockerhub. 

This process is automated using GitLab-CI and I used the following [guided exploration](https://gitlab.com/guided-explorations/containers/kaniko-docker-build) as the main source to prepare my .gitlab-ci file. Note that in this project:

- Kaniko cache is disabled (but can be enabled by changing the `KANIKO_CACHE_ARGS` variable.
- The image tags do not include the latest Git version (but can be changed with `VERSIONLABELMETHOD` variable)
- No CI/CD variables are necessary for pushing the GitLab project's own registry but following variables need to be set in order to push to the DockerHub:

  | Type     | Key                  | Value                       | Protected | Masked | Scope              |
  | -------- | -------------------- | --------------------------- | --------- | ------ | ------------------ |
  | Variable | CI_REGISTRY          | https://index.docker.io/v1/ | No        | No     | push-to-docker-hub |
  | Variable | CI_REGISTRY_IMAGE    | docker.io/youruser/yourrepo | No        | No     | push-to-docker-hub |
  | Variable | CI_REGISTRY_PASSWORD | yourpassword                | No        | Yes    | push-to-docker-hub |
  | Variable | CI_REGISTRY_USER     | Yourdockeruserid            | No        | No     | push-to-docker-hub |
